﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using XmlImporter.Models;

namespace XmlImporter
{
    /// <summary>
    /// Class to handle the XML import job
    /// </summary>
    public class Importer
    {
        private XmlTextReader _reader;

        /// <summary>
        /// Filename to import from. Must be set before calling the Run() method
        /// </summary>
        public string File { set; get; }

        /// <summary>
        /// Run the import job
        /// </summary>
        public void Run()
        {
            if (string.IsNullOrEmpty(File))
            {
                throw new Exception("Filename is not set");
            }

            var myFamily = new Family();

             _reader = new XmlTextReader(File);

            var name = new Name();
            while (_reader.Read())
            {
                if((_reader.NodeType == XmlNodeType.Element && _reader.Name.Equals("PERSON")))
                {
                    // Read the id
                    var id = AdvanceAndReadAttribute("idno");

                    // Read the sex directly 
                    var sex = AdvanceAndRead("SEX");

                    // Read the name object 
                    AdvanceReader( "NAME");
                    if (_reader.NodeType == XmlNodeType.Element && _reader.Name.Equals("NAME"))
                    {
                        name = ParseName();                       
                    }


                    var person = new Person();
                    myFamily.Add(new Person{Id = id, Name = name, Sex = sex == "M" ? Sex.Male : Sex.Female}); // Add the person
                }                 
            }
        }

        /// <summary>
        /// Parse the name
        /// </summary>
        /// <returns>A name object</returns>
        public Name ParseName()
        {          
            return new Name
                {
                    GivenName = AdvanceAndRead("GIVEN_NAMES"),
                    FamilyName = AdvanceAndRead("FAMILY_NAME")
                };
        }

        /// <summary>
        /// Advance the _reader and read the element value
        /// </summary>
        /// <param name="elementName">Element to look for</param>
        /// <returns></returns>
        public string AdvanceAndRead(string elementName)
        {
            AdvanceReader(elementName);
            return ReadValue( elementName);
        }

        /// <summary>
        /// Advance the _reader and read the element value
        /// </summary>
        /// <param name="attributeName">Attribute to look for</param>
        /// <returns></returns>
        public string AdvanceAndReadAttribute(string attributeName)
        {
            while (_reader.Name != attributeName)
            {
                _reader.MoveToNextAttribute();
            }

            return ReadAttributeValue(attributeName);
        }

        /// <summary>
        /// Advance the _reader by looping until the name matches the elementName
        /// </summary>
        /// <param name="elementName">Element to look for</param>
        public void AdvanceReader(string elementName)
        {
            while (!_reader.Name.Equals(elementName))
            {
                _reader.Read();
            }
        }

        /// <summary>
        /// Read the value of the current node in the _reader
        /// </summary>
        /// <param name="elementName">Element to read, used for extra security check</param>
        /// <returns>The value if found else emptry string</returns>
        public string ReadValue(string elementName)
        {
            if (_reader.NodeType == XmlNodeType.Element && _reader.Name.Equals(elementName))
            {
                _reader.Read();
                return _reader.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Read the value of the current node in the _reader
        /// </summary>
        /// <param name="attributeName">Attribute to read, used for extra security check</param>
        /// <returns>The value if found else emptry string</returns>
        public string ReadAttributeValue(string attributeName)
        {
            if (_reader.NodeType == XmlNodeType.Attribute && _reader.Name.Equals(attributeName))
            {
                return _reader.Value;
            }

            return string.Empty;
        }
    }
}
