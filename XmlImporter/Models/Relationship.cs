﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlImporter.Models
{
    /// <summary>
    /// Models a relationship 
    /// </summary>
    public class Relationship
    {
        private int _linkto;

        /// <summary>
        /// The name of person
        /// </summary>
        public Name Name { get; set; }

        /// <summary>
        /// The person in the given relationship (do not use in import job in phase one)
        /// </summary>
        public Person Person { set; get; }

        /// <summary>
        /// The type of relation type
        /// </summary>
        public RelationType Type { set; get; }

        /// <summary>
        /// Set the linked to using an input string
        /// </summary>
        public string LinkTo
        {
            set
            {
                var val = value;
                if (!val.Contains("I")) return;

                int outId;
                if (int.TryParse(val.Split('-')[0].Substring(1), out outId))
                {
                    _linkto = outId;
                }
            }
        }

        /// <summary>
        /// Get the internal linked to id
        /// </summary>
        public int LinkId { get { return _linkto; } }
       
        /// <summary>
        /// The type of relationship a person can have
        /// </summary>
        public enum RelationType
        {
            /// <summary>
            /// Parent
            /// </summary>
            ParentOf,
            /// <summary>
            /// Child of father
            /// </summary>
            ChildOfFather,
            /// <summary>
            /// Child of mother
            /// </summary>
            ChildOfMother,
            /// <summary>
            /// Sibling
            /// </summary>
            SiblingTo,
            /// <summary>
            /// Spouse of
            /// </summary>
            SpouseOf
        }
    }
}
