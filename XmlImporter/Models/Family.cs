﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlImporter.Models
{
    /// <summary>
    /// Models a family
    /// </summary>
    class Family : List<Person>
    {
        public Person FindById(int id)
        {
            return this.Find(x => x.InteralId == id);
        }
    }
}
