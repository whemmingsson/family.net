﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlImporter.Models
{
    /// <summary>
    /// Models a special event in a persons life
    /// </summary>
    public class Event
    {
        private EventType _type;
 
        /// <summary>
        /// The type of event
        /// </summary>
        public string Type 
        {
            set
            {
                switch (value)
                {
                    case "Marriage": _type = EventType.Marriage;
                        break;
                    case "Birth": _type = EventType.Birth;
                        break;
                    case "Death": _type = EventType.Death;
                        break;
                }
            }
        }

        /// <summary>
        /// The date of the event
        /// </summary>
        public string Date { set; get; }

        /// <summary>
        /// The place of the event
        /// </summary>
        public string Place { set; get; }

        /// <summary>
        /// Models the type of event
        /// </summary>
        public enum EventType
        {
            Birth, Marriage, Death
        }
    }
}
