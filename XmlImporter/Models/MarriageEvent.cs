﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlImporter.Models
{
    /// <summary>
    /// Models a marriage event 
    /// </summary>
    class MarriageEvent : Event
    {
        private Person _refPerson;

        /// <summary>
        /// Creates the marriage that also has a reference to the refering person
        /// </summary>
        /// <param name="refererPerson">The owner of this marriage event</param>
        public MarriageEvent(Person refererPerson)
        {
            _refPerson = refererPerson;
        }

        /// <summary>
        /// Returns the type eg marriage
        /// </summary>
        public new EventType Type
        {
            get{ return EventType.Marriage; }
        }

        /// <summary>
        /// Strongly typed person in the marriage
        /// </summary>
        public Person Person { get; set; }

        /// <summary>
        /// Losely typed person in the marriage
        /// </summary>
        public Name Name { get; set; }
    }
}
