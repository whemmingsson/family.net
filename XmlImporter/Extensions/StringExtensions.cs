﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlImporter.Extensions
{
    /// <summary>
    /// String extensions
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Fixes the character encoding error
        /// </summary>
        /// <param name="source">String source</param>
        /// <returns>Fixed string</returns>
        public static string SweFix(this string source)
        {
            return source.Replace("èo", "ö").Replace("èa", "ä").Replace("êa", "å");
        }

        /// <summary>
        /// Fixes the string ending 
        /// </summary>
        /// <param name="source">String source</param>
        /// <returns>Fixed string</returns>
        public static string EndFix(this string source)
        {
            return source.TrimEnd('\n').TrimEnd('\r').TrimEnd(' ');
        }


    }
}
