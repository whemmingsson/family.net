﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlImporter
{
    /// <summary>
    /// Main program
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main entry
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var xmlImporter = new XmlParser();
            xmlImporter.File = "source.xml";
            xmlImporter.Run();

            Console.ReadKey();
        }
    }
}
