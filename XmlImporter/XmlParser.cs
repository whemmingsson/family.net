﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using XmlImporter.Extensions;
using FamilyModels;


namespace XmlImporter
{
    /// <summary>
    /// Class to handle the XML import job
    /// </summary>
    public class XmlParser
    {
        /// <summary>
        /// Filename to import from. Must be set before calling the Run() method
        /// </summary>
        public string File { set; get; }

        /// <summary>
        /// Run the import job
        /// </summary>
        public Family Run()
        {
            // Import to the schema class
            var serializer = new XmlSerializer(typeof(FAMILY_TREE));
            var family = serializer.Deserialize(new FileStream(File, FileMode.Open)) as FAMILY_TREE;
            var realFamily = ConvertToUserFriendlyObject(family);

            // Link the persons
            LinkPersonsInRelationships(realFamily);

            return realFamily;
        }

        /// <summary>
        /// Link the persons specified by the ID in relationships with others
        /// </summary>
        /// <param name="family">The family to link for</param>
        private void LinkPersonsInRelationships(Family family)
        {
            //foreach (var person in family)
            //{
            //    var allRelations = person.Relations;

            //    foreach (var relationship in allRelations)
            //    {
            //        var id = relationship.LinkId;
            //        var rPerson = family.FindById(id);
            //        if (rPerson != null)
            //        {
            //            relationship.Person = rPerson;
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Converts the family from the auto-generated class to user friendly objects
        /// </summary>
        /// <param name="family"></param>
        /// <returns></returns>
        private Family ConvertToUserFriendlyObject(FAMILY_TREE family)
        {
            var superModel = new FamilyModelHolder();

            var parsedFamily = new Family();
            var parsedRelations = new Relations();
            var parsedMarriages = new Marriages();
            
            foreach (var person in family.PERSON)
            {
                // User friendly person
                var ufPerson = new Person();

                // Simple properties
                ufPerson.SetId(person.idno);
                ufPerson.Name = new Name { FamilyName = person.NAME.FAMILY_NAME.SweFix().EndFix(), GivenName = person.NAME.GIVEN_NAMES.SweFix().EndFix() };           
                ufPerson.Sex = person.SEX == FAMILY_TREEPERSONSEX.F ? Sex.Female : Sex.Male;
               
                // Complex properties
                if (person.Items != null)
                {
                    foreach (var item in person.Items)
                    {
                        // Add event
                        if (item is FAMILY_TREEPERSONEVENT)
                        {
                            var e = item as FAMILY_TREEPERSONEVENT;

                            var personEvent = new Event
                                {
                                    Date = e.DATE != null ? (e.DATE as XmlNode[])[0].Value.SweFix().EndFix() : "",
                                    Place = e.PLACE != null ? (e.PLACE as XmlNode[])[0].Value.SweFix().EndFix() : "",
                                    Type = e.type.ToString(),
                                    Id = int.Parse(e.family.Substring(1))
                                };

                            //TODO: Fill out with all the other types of events
                            if (e.type == FAMILY_TREEPERSONEVENTType.Birth)
                            {
                                ufPerson.Birth = personEvent;
                            }

                            if (e.type == FAMILY_TREEPERSONEVENTType.Death)
                            {
                                ufPerson.Death = personEvent;
                            }

                            // Special person based event that endsup in own table
                            if (e.type == FAMILY_TREEPERSONEVENTType.Marriage)
                            {
                                var mEvent = new MarriageEvent
                                    {
                                        FirstPersonId = ufPerson.Id,
                                        SecondPersonId = -1,
                                        Date = personEvent.Date,
                                        Place = personEvent.Place,
                                        Id = personEvent.Id
                                    };

                                parsedMarriages.Add(mEvent);
                            }
                            
                        }

                        // Add relation
                        if (item is FAMILY_TREEPERSONRELATIONSHIP)
                        {
                            var r = item as FAMILY_TREEPERSONRELATIONSHIP;

                            var ufRName = new Name { FamilyName = r.NAME.FAMILY_NAME.SweFix().EndFix(), GivenName = r.NAME.GIVEN_NAMES.SweFix().EndFix() };
                            var ufRLinkto = r.linkto;
                            var ufRType = new Relationship.RelationType();

                            switch (r.type)
                            {
                                case FAMILY_TREEPERSONRELATIONSHIPType.ParentOf:
                                    ufRType = Relationship.RelationType.ParentOf;
                                    break;
                                case FAMILY_TREEPERSONRELATIONSHIPType.SpouseOf:
                                    ufRType = Relationship.RelationType.SpouseOf;
                                    break;
                                case FAMILY_TREEPERSONRELATIONSHIPType.ChildOfFather:
                                    ufRType = Relationship.RelationType.ChildOfFather;
                                    break;
                                case FAMILY_TREEPERSONRELATIONSHIPType.ChildOfMother:
                                    ufRType = Relationship.RelationType.ChildOfMother;
                                    break;
                            }

                            ufPerson.AddRelation(new Relationship {Name = ufRName, Type = ufRType, LinkTo = ufRLinkto});
                        }
                    }
                }

                parsedFamily.Add(ufPerson);
                
            }

            superModel.Family = parsedFamily;
            superModel.Marriages = parsedMarriages;
            superModel.Relations = parsedRelations;

            return parsedFamily;
        }       
    }
}

       
