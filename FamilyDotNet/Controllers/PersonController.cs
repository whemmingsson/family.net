﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FamilyDotNet.Models;
using FamilyModels;
using XmlImporter;

namespace FamilyDotNet.Controllers
{
    public class PersonController : Controller
    {
        private FamilyDb _db = new FamilyDb();
 
        /// <summary>
        /// Index action
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var parser = new XmlParser();
            parser.File = "source.xml";

            var family = parser.Run();

            // add data into context and save to db
            foreach (Person p in family)
            {
                _db.Persons.Add(p);
            }

            _db.SaveChanges();

            var model = _db.Persons.ToList();

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
