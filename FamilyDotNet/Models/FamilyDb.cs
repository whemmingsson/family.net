﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using FamilyModels;
using XmlImporter;

namespace FamilyDotNet.Models
{
    public class FamilyDb : DbContext
    {
        public DbSet<Person> Persons { set; get; }

    }

    public class EntitiesContextInitializer : DropCreateDatabaseAlways<FamilyDb>
    {
        protected override void Seed(FamilyDb context)
        {


            var parser = new XmlParser();
            parser.File = "source.xml";

            var family = parser.Run();


            // add data into context and save to db
            foreach (Person p in family)
            {
                context.Persons.Add(p);
            }
            context.SaveChanges();

        }
    }
}