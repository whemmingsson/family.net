﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyModels
{
    /// <summary>
    /// Main model holder class used for storing result from import job
    /// </summary>
    public class FamilyModelHolder
    {
        /// <summary>
        /// Public contructor
        /// </summary>
        public FamilyModelHolder()
        {
            Family = new Family();
            Marriages = new Marriages();
            Relations = new Relations();
        }

        /// <summary>
        /// All persons eg the family
        /// </summary>
        public Family Family { get; set; }

        /// <summary>
        /// All marriages
        /// </summary>
        public Marriages Marriages { get; set; }

        /// <summary>
        /// All relations
        /// </summary>
        public Relations Relations { get; set; }
    }
}
