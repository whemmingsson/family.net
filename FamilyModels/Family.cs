﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyModels
{
    /// <summary>
    /// Models a family
    /// </summary>
    public class Family : List<Person>
    {
        public Person FindById(int id)
        {
            return this.Find(x => x.Id == id);
        }
    }
}
