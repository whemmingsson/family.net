﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyModels
{
    /// <summary>
    /// New way of modeling the relations 
    /// </summary>
    public class Relation
    {
        /// <summary>
        /// The family ID
        /// </summary>
        public int Id { set; get; }

        /// <summary>
        /// The "first person" eg the childof, spouse of or parent of
        /// </summary>
        public int FirstPersonId { get; set; }

        /// <summary>
        /// The second person
        /// </summary>
        public int SecondPersonId { get; set; }

        /// <summary>
        /// The type of relation
        /// </summary>
        public Relationship.RelationType Type { set; get; }
    }
}
