﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyModels
{
    /// <summary>
    /// Models a name
    /// </summary>
    public class Name
    {
        /// <summary>
        /// A given name, eg the sure name
        /// </summary>
        public string GivenName { set; get; }

        /// <summary>
        /// A family name, eg the last name
        /// </summary>
        public string FamilyName { set; get; }
    }
}
