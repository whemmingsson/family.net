﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyModels
{
    /// <summary>
    /// Sex of a person
    /// </summary>
    public enum Sex
    {
        /// <summary>
        /// Male 
        /// </summary>
        Male, 
        
        /// <summary>
        /// Female
        /// </summary>
        Female
    }
}
