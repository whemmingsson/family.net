﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyModels
{
    /// <summary>
    /// Models a marriage event 
    /// </summary>
    public class MarriageEvent : Event
    {
        public int FirstPersonId { get; set; }
        public int SecondPersonId { get; set; }
    }
}
