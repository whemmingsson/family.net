﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyModels
{
    /// <summary>
    /// Models a person
    /// </summary>
    public class Person
    {
        #region Fields

        private readonly List<Relationship> _relations;
        private readonly List<Event> _events;
        private int _internalId;

        #endregion

        #region Constructors

        /// <summary>
        /// Public (default) constructor
        /// </summary>
        public Person()
        {
            _relations = new List<Relationship>();
            _events = new List<Event>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The XML-based ID is used to parse into the internal id. Only used by the import job
        /// </summary>
        public void SetId(string id)
        { 
            var val = id;
            if (!val.Contains("I")) return;

            int outId;
            if (int.TryParse(val.Substring(1), out outId))
            {
                _internalId = outId;
            }  
        }

        /// <summary>
        /// Id of the person
        /// </summary>
        public int Id { set { _internalId = value; } get { return _internalId; } }

        /// <summary>
        /// Sex of the person
        /// </summary>
        public Sex Sex { set; get; }

        /// <summary>
        /// Name of the person
        /// </summary>
        public Name Name { set; get; }

        /// <summary>
        /// The birth event of the person
        /// </summary>
        public Event Birth { set; get; }

        /// <summary>
        /// Death event of the person
        /// </summary>
        public Event Death { set; get; }

        /// <summary>
        /// Parses the name into a quick representation of the whole name
        /// </summary>
        public string QuickName
        {
            get
            {
                if (Name != null)
                {
                    return Name.GivenName + " " + Name.FamilyName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// All persons relations
        /// </summary>
        //public List<Relationship> Relations { get { return _relations; } } 

        #endregion

        #region Methods

        /// <summary>
        /// Add a relationship object to the internal list of relations
        /// </summary>
        /// <param name="relation">The new relation</param>
        public void AddRelation(Relationship relation)
        {
            _relations.Add(relation);
        }
      
        /// <summary>
        /// Add a strongly typed relation to another existing person
        /// </summary>
        /// <param name="person">The related person</param>
        /// <param name="type">The type of relation</param>
        public void AddRelation(Person person, Relationship.RelationType type)
        {
            _relations.Add(new Relationship {Person = person, Type = type});
        }

        /// <summary>
        /// Add a loosley typed relation to another existing person
        /// </summary>
        /// <param name="name">The related persons name</param>
        /// <param name="type">The type of relation</param>
        public void AddRelation(Name name, Relationship.RelationType type)
        {
            _relations.Add(new Relationship { Name=name, Type = type });
        }

        /// <summary>
        /// Add an event to the person
        /// </summary>
        /// <param name="eEvent">The event to add</param>
        public void AddEvent(Event eEvent)
        {
            _events.Add(eEvent);
        }

        /// <summary>
        /// Add an event using parameters
        /// </summary>
        /// <param name="type">Type of event</param>
        /// <param name="date">Date of the event</param>
        /// <param name="place">Place of the event</param>
        [Obsolete("[Deprecated]")]
        public void AddEvent(Event.EventType type, string date, string place)
        {
            throw new NotImplementedException("This method is deprecated");
        }

        #endregion
    }
}
